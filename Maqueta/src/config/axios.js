import axios from 'axios';

const baseURL = 'http://localhost:7171/api';

const clienteAxios = axios.create({ baseURL });

export default clienteAxios;