import { CLIENTE_ADD, CLIENTE_DELETE, CLIENTE_ERROR, CLIENTE_GET, CLIENTE_START, CLIENTE_SUCCESS } from "../types";

const initialState = {
  clientes: [],
  clienteEdit: null,
  error: false,
  loading: false,
  sending: false,
};

// eslint-disable-next-line
export default (state = initialState, action) => {
  switch (action.type) {
    case CLIENTE_START:
      return {
        ...state,
        loading: true,
        error: false,
        sending: false,
        clienteEdit: null,
      };
    case CLIENTE_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        sending: false,
        clienteEdit: null,
      };
    case CLIENTE_SUCCESS:
      return {
        ...state,
        loading: false,
        sending: true,
        error: false,
        clientes: action.payload,
      };
  
    case CLIENTE_ADD:
      return {
        ...state,
        loading: false,
        error: false,
        clienteEdit: null,
        sending: true,
      };
    case CLIENTE_DELETE:
      return {
        ...state,
        loading: false,
        error: false,
        clientes: state.clientes.filter(
          (p) => p.id !== action.payload
        ),
      };
    case CLIENTE_GET:
      return {
        ...state,
        loading: false,
        error: false,
        clienteEdit: action.payload,
      };
    default:
      return state;
  }
};
