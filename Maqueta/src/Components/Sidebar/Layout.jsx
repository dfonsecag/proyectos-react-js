import React, { useState } from "react";
import Header from "./Header";
import Aside from "./Aside";

function Layout(props) {
  const [rtl, setRtl] = useState(false);
  const [toggled, setToggled] = useState(false);

 
  const handleToggleSidebar = (value) => {
    setToggled(value);
  };

  return (
    <div className={`app ${rtl ? "rtl" : ""} ${toggled ? "toggled" : ""}`}>
      <Aside
        rtl={rtl}
        toggled={toggled}
        handleToggleSidebar={handleToggleSidebar}
      />
      <main>
        <Header handleToggleSidebar={handleToggleSidebar} title={props.title}/>
        <div className="app-content">{props.children}</div>
      </main>
    </div>
  );
}

export default Layout;
