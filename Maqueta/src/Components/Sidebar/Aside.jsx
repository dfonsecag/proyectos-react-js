import React from "react";
import { useIntl } from "react-intl";
import { NavLink } from "react-router-dom";
// import
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarFooter,
  SidebarContent
} from "react-pro-sidebar";
import {
  FaTachometerAlt,
  FaGem,
  FaRegLaughWink
} from "react-icons/fa";
import "react-pro-sidebar/dist/css/styles.css";
import { useLocation } from "react-router-dom";

import logo from "./../../../src/assets/forestal.png";

const Aside = ({ rtl, toggled, handleToggleSidebar }) => {
  const intl = useIntl();
  const location = useLocation();
  
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <ProSidebar
      rtl={rtl}
      toggled={toggled}
      breakPoint="md"
      onToggle={handleToggleSidebar}
    >
      <SidebarHeader>
        <div className="sidebar-header">
          <p className="user-name text-white mb-2">Diego García Fonseca</p>
          <p className="user-email">dgf-95@hotmail.com</p>
          <div className="d-flex justify-content-center">
            <div className="position-absolute bottom-0">
              <img
                src={logo}
                alt="User profile"
                className="user-image"
              />
            </div>
          </div>
        </div>
      </SidebarHeader>

      <SidebarContent>
        <Menu iconShape="circle">
          <MenuItem
            icon={<FaTachometerAlt />}
            suffix={
              <span className="badge red">
                {intl.formatMessage({ id: "new" })}
              </span>
            }
          >
            <NavLink exact to={"/"}>
              {intl.formatMessage({ id: "dashboard" })}
            </NavLink>
          </MenuItem>
          <MenuItem icon={<FaGem />}>
            Componentes
          </MenuItem>
        </Menu>

        {/* With Suffix */}
        <Menu iconShape="circle" >
          <SubMenu
            suffix={<span className="badge yellow">2</span>}
            title="Módulo Clientes"
            icon={<FaRegLaughWink />}
            data-element={location.pathname}
            open={true}
            onClose={handleClose}
          >
            <MenuItem  icon={<FaRegLaughWink />}>
              <NavLink exact to={"/clients"}>
                Clientes
              </NavLink>
            </MenuItem>
            <MenuItem>
              <NavLink exact to={"/create_client"}>
               Crear Cliente
              </NavLink>
            </MenuItem>
            
          </SubMenu>
         
          
        </Menu>
      </SidebarContent>

      <SidebarFooter style={{ textAlign: "center" }}>
        <div
          className="sidebar-btn-wrapper"
          style={{
            padding: "20px 24px"
          }}
        >
         Copyright © Diego García Fonseca
        </div>
      </SidebarFooter>
    </ProSidebar>
  );
};

export default Aside;
