import React from "react";

export const Date = ({name,label,onChange,value,offset=1,columns}) => {
  return (
    <div className={`col-md-${columns} offset-md-${offset}`}>
      <label htmlFor={name} className="form-label">
        {label}
      </label>
      <input
        type="date"
        className="form-control"
        id={name}
        name={name}
        onChange={onChange}
        value={value}
      />
    </div>
  );
};
