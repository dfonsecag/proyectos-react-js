import React from "react";

export const Email = ({ name, label, onChange, value, columns, offset=1 }) => {
  return (
    <>
      <div className={`col-md-${columns} offset-md-${offset}`}>
        <label htmlFor="validationCustomUsername" className="form-label">
          {label}
        </label>
        <div className="input-group has-validation">
          <span className="input-group-text" id="inputGroupPrepend">
            @
          </span>
          <input
            type="email"
            className="form-control"
            id={name}
            name={name}
            onChange={onChange}
            value={value}
          />
        </div>
      </div>
    </>
  );
};
