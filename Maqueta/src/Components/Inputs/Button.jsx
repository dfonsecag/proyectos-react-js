import React from "react";
import IconButton from "@material-ui/core/IconButton";
import Delete from "@material-ui/icons/Delete";
import Edit from "@material-ui/icons/Edit";

const Button = ({ handleClick, icon }) => {
  return (
    <IconButton
    aria-label="account of current user"
    aria-controls="menu-appbar"
    aria-haspopup="true"
    onClick={handleClick}
    color="inherit"
  >
    {
      icon == 'Edit' ?
      ( <Edit />) :
      ( <Delete /> )
    }
   
  </IconButton>
  );
};

export default Button;
