import Button from "../Inputs/Button";

export const PropertyData = (info, deleteCliente, getPropertyType) => {
  const data = {
    columns: [
      {
        label: "Cédula",
        field: "cedula",
        width: 150,
      },
      {
        label: "Nombre Completo",
        field: "nombre",
        width: 150,
      },
      {
        label: "Fecha Nacimiento",
        field: "fechaNacimiento",
        width: 150,
      },
      {
        label: "Correo",
        field: "correo",
        width: 150,
      },
      {
        label: "Teléfono",
        field: "telefono",
        width: 150,
      },
      {
        label: "Acciones",
        field: "accion",
        width: 150,
      },
      
    ],
    rows:
      info.length > 0
        ? info.map((e) => ({
            cedula: e.cedula,
            nombre: e.nombre,
            fechaNacimiento: e.fechanacimiento,
            correo: e.correo,
            telefono: e.telefono,
            accion: (
              <div className="d-flex">
                <Button
                  titulo={"Editar"}
                  handleClick={() => getPropertyType(e)}
                  icon="Edit"
                  type="Success"
                />
                &nbsp;
                <Button
                  titulo={"Eliminar"}
                  handleClick={() => deleteCliente(e.id,e.nombre)}
                  icon="Delete"
                  type="danger"
                />
              </div>
            ),
          }))
        : [],
  };
  return data;
};
