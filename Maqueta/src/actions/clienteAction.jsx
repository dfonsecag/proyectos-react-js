import Swal from "sweetalert2";
import clienteAxios from "../config/axios";

import { CLIENTE_ADD, CLIENTE_CLEAR, CLIENTE_DELETE, CLIENTE_ERROR, CLIENTE_GET, CLIENTE_START, CLIENTE_SUCCESS } from "../types";

// Obtiene todos los clientes
export const getClienteAction = () => {
  return async (dispatch) => {
    dispatch(clienteStart());
    try {
      const response = await clienteAxios.get(`/clientes`);
      dispatch(clienteSuccess(response.data));
    } catch (error) {
      dispatch(clienteError());
    }
  };
};

// Obtiene un cliente en especifico por Id
export const getClienteIdAction = (id) => {
  return async (dispatch) => {
    dispatch(clienteStart());
    try {
      const response = await clienteAxios.get(`/clientes/${id}`);
      dispatch(getClienteSuccess(response.data));
    } catch (error) {
      dispatch(clienteError());
    }
  };
};

// Agregar o editar  un cliente
export const addClienteAction = (data,edit, id) => {
  return async (dispatch) => {
    dispatch(clienteStart());
    try {
      if (!edit) {
        await clienteAxios.post(`/clientes`, data);
        Swal.fire({
          icon: "success",
          title: "Registro con éxito.",
          text: "Se ha registrado un nuevo cliente exitosamente.",
          confirmButtonText: "Aceptar",
        });
      } else {
        await clienteAxios.put(`/clientes/${id}`, data);
        Swal.fire({
          icon: "success",
          title: "Actualizado con éxito.",
          text: "Se ha actualizado un cliente exitosamente.",
          confirmButtonText: "Aceptar",
        });
      }
      dispatch(getClienteAction())
      dispatch(addClienteSuccess());
    } catch (error) {
      dispatch(clienteError());
    }
  };
};

// Eliminar un cliente
export const deleteClienteAction = (id) => {
  return async (dispatch) => {
    dispatch(clienteStart());
    try {
      await clienteAxios.delete(`/clientes/${id}`);
      dispatch(deleteClienteSuccess(id));
      Swal.fire({
        icon: "success",
        title: "Eliminado con éxito.",
        text: "Se ha eliminado un cliente exitosamente.",
        confirmButtonText: "Aceptar",
      });
      
    } catch (error) {
      console.log(error)
      dispatch(clienteError());
      Swal.fire({
        icon: "error",
        title: "Hubo un error !",
        text: "Intente nuevamente",
        confirmButtonText: "Aceptar",
      });
    }
  };
};



export const clienteStart = () => ({
  type: CLIENTE_START,
});

export const clienteSuccess = (data) => ({
  type: CLIENTE_SUCCESS,
  payload: data,
});

export const clienteError = () => ({
  type: CLIENTE_ERROR,
});

export const addClienteSuccess = () => ({
  type: CLIENTE_ADD,
});

export const deleteClienteSuccess = (id) => ({
  type: CLIENTE_DELETE,
  payload: id,
});

export const getClienteSuccess = (data) => ({
  type: CLIENTE_GET,
  payload: data,
});

export const clearOwnerAction = () => ({
  type: CLIENTE_CLEAR,
});