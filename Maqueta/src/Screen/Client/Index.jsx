import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import IconButton from "@material-ui/core/IconButton";
import Delete from "@material-ui/icons/Delete";
import Edit from "@material-ui/icons/Edit";
import { PropertyData } from "../../Components/variables";
import Datatable from "../../Components/datatable/Datatable";
import Spinner from "../../Components/Spinner/SpinnerCircular";
import Swal from "sweetalert2";
import { useHistory } from "react-router-dom";
import { getClienteAction,deleteClienteAction } from "../../actions/clienteAction";
import Layout from "../../Components/Sidebar/Layout";

const Cliente = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const clientesRequest = useSelector((state) => state.clientes.clientes);

  const loading = useSelector((state) => state.clientes.loading);


  useEffect(() => {
    dispatch(getClienteAction());
    // eslint-disable-next-line
  }, []);

  /**Eliminar propiedad */
  const deleteCliente = (id,nombre) => {
    Swal.fire({
      title: "¿Estás seguro?",
      text: `El cliente ${nombre} se eliminará.`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, eliminar!",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteClienteAction(id));
      }
    });
  };

  /**Obtener informacion de la fila a editar */
  const getClienteId = (row) => {
    history.push(`cliente-edit/${row.id}`);
  };

  const datatable = PropertyData(
    clientesRequest,
    deleteCliente,
    getClienteId
  );

  return (
    <Layout title="Vista Clientes">
      <div className="row">
      
        <div className="col mx-auto">
          <div className="row">
           
            <div className="datatable-container col-11 mx-auto mt-4">
              {loading ? (
                <Spinner texto={"Cargando"} />
              ) : (
                <Datatable datos={datatable} />
              )}
            </div>
          </div>
        </div>
      </div>
    
    </Layout>
  );
};

export default Cliente;
