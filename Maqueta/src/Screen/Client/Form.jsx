import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addClienteAction,
  getClienteIdAction,
} from "../../actions/clienteAction";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { Email } from "../../Components/Inputs/Email";
import { Input } from "../../Components/Inputs/Input";
import Layout from "../../Components/Sidebar/Layout";
import { Date } from "../../Components/Inputs/Date";

const CreateClient = () => {
  // Parámetro del id
  const { id } = useParams();

  // Historial del router
  const history = useHistory();

  // Dispara una acción en el action
  const dispatch = useDispatch();

  // Validar
  const [error, setError] = useState(false);
  const [errores, setErrores] = useState([]);

  // estado para si es un formulario para editar o crear
  const [edit, setEdit] = useState(false);

  const [cliente, setCliente] = useState({
    nombre: "",
    primerApellido: "",
    segundoApellido: "",
    cedula: "",
    fechaNacimiento: "",
    correo: "",
    telefono: "",
  });

  const onChange = (e) => {
    setCliente({
      ...cliente,
      [e.target.name]: e.target.value,
    });
  };

  // Estado que en el reducer pasa a true si fue enviado
  const sending = useSelector((state) => state.clientes.sending);

  // Obtiene los datos si se esta editando un cliente
  const clienteEdit = useSelector((state) => state.clientes.clienteEdit);

  useEffect(() => {
    if (id !== undefined) {
      setEdit(true);
      dispatch(getClienteIdAction(id))
    } else {
      setEdit(false);
      
      clearForm();
    }
    // eslint-disable-next-line
  }, [id]);

  useEffect(() => {
    if(clienteEdit !== null){
      clienteEdit.fechaNacimiento = clienteEdit.fechaNacimiento.substring(0,10);
      setCliente(clienteEdit)
    } 
    // eslint-disable-next-line
  }, [clienteEdit]);


  useEffect(() => {
    if (sending) {
      clearForm();
    }

    // eslint-disable-next-line
  }, [sending]);

  const addClient = (e) => {
    e.preventDefault();
    let arregloErrores = [];
    if (nombre === "") {
      arregloErrores.push("El nombre es requerido");
    }
    if (primerApellido === "") {
      arregloErrores.push("El primer apellido es requerido");
    }
    if (segundoApellido === "") {
      arregloErrores.push("El segundo apellido es requerido");
    }
    if (correo === "") {
      arregloErrores.push("El correo es requerido");
    }
    if (cedula === "") {
      arregloErrores.push("La cédula es requerida");
    }
    if (fechaNacimiento === "") {
      arregloErrores.push("La fecha de nacimiento es requerida");
    }
    if (telefono === "") {
      arregloErrores.push("El teléfono es requerido");
    }

    if (arregloErrores.length > 0) {
      setErrores(arregloErrores);
      setError(true);
    } else {
      setErrores([]);
      setError(false);
      dispatch(addClienteAction(cliente, edit, id));
       if(edit){
        history.push(`/clients`);
       }
    }
  };

  const clearForm = () => {
    setCliente({
      nombre: "",
      primerApellido: "",
      segundoApellido: "",
      cedula: "",
      fechaNacimiento: "",
      correo: "",
      telefono: "",
    });
  };

  const {
    nombre,
    primerApellido,
    segundoApellido,
    cedula,
    fechaNacimiento,
    correo,
    telefono,
  } = cliente;

  return (
    <Layout title={edit ? 'Editar Cliente' : 'Crear Cliente'}>
      <>
        {error && (
          <div
            className="alert alert-danger col-md-12 offset-md-1"
            role="alert"
          >
            <ul>
              {errores.map((item, i) => (
                <li key={i}>{item}</li>
              ))}
            </ul>
          </div>
        )}

        <form className="row g-3 needs-validation" onSubmit={addClient}>
          {/* Input de Nombre */}
          <Input
            name="nombre"
            onChange={onChange}
            value={nombre}
            columns={4}
            label="Nombre"
          />

          <Input
            name="primerApellido"
            onChange={onChange}
            value={primerApellido}
            columns={4}
            label="Primer Apellido"
          />

          <Input
            name="segundoApellido"
            onChange={onChange}
            value={segundoApellido}
            columns={4}
            label="Segundo Apellido"
          />

          <Email
            name="correo"
            type="email"
            onChange={onChange}
            value={correo}
            columns={4}
            label="Correo Electrónico"
          />

          <Input
            name="cedula"
            onChange={onChange}
            value={cedula}
            columns={4}
            label="Cédula"
          />

          <Date
            name="fechaNacimiento"
            onChange={onChange}
            value={fechaNacimiento}
            columns={4}
            label="Fecha Nacimiento"
          />

          <Input
            name="telefono"
            onChange={onChange}
            value={telefono}
            columns={4}
            label="Teléfono"
          />

          <div className="col-12 text-center  mb-6">
            <button className="btn btn-success" type="submit">
              { edit ? 'Editar ' : ' Crear '} cliente             
            </button>
          </div>
        </form>
      </>
    </Layout>
  );
};

export default CreateClient;
