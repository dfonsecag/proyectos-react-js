import React from "react";
import About from "../src/Screen/About";
import { Switch, Route } from "react-router-dom";
import Form from "./Screen/Client/Form";
import IndexClient from "./Screen/Client/Index";

const Routes = () => {
  return (
    <>
      <Switch>
        <Route exact path={"/"} component={About}/>
        <Route exact path={"/create_client"} component={Form}/>
        <Route exact path={"/cliente-edit/:id"} component={Form}/>
        <Route exact path={"/clients"} component={IndexClient}/>
        <Route exact path={"/about"}>
          <About />
        </Route>
      </Switch>
    </>
  );
};

export default Routes;
