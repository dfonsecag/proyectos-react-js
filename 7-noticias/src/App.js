import React, { Fragment, useState, useEffect } from "react";

import Header from "./components/Header";
import Formulario from "./components/formulario/Formulario";
import ListadoNoticias from "./components/ListadoNoticias";

function App() {
  // definir la categorias y noticias
  const [categoria, guardarCategoria] = useState("");

  //state de noticias
  const [noticias, guardarNoticias] = useState([]);

  useEffect(() => {
    //Funcion para consultar a la api
    const consultarAPI = async () => {
      const url = `https://newsapi.org/v2/top-headlines?country=mx&category=${categoria}&apiKey=d7f4c809eb1643939c0b81532179c32a`;
      const respuesta = await fetch(url);
      const noticias = await respuesta.json();
      //cargo al estado las noticias
      guardarNoticias(noticias.articles);
    };
    consultarAPI();
  }, [categoria]);

  return (
    <Fragment>
      <Header titulo="Buscador de Noticias" />

      <div className="container white ">
        <Formulario guardarCategoria={guardarCategoria} />

        <ListadoNoticias
          noticias={noticias}
        />

      </div>
    </Fragment>
  );
}

export default App;
