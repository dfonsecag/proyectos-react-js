import React from 'react'
import PropTypes from 'prop-types';
import styles from './Formulario.module.css';

import useSelect from '../../hooks/useSelect';

const Formulario = ({guardarCategoria}) => {

    //crear opciones de la categoria de noticias
    const OPCIONES = [
        {value: 'general', label:'GENERAL'},
        {value: 'business', label:'NEGOCIOS'},
        {value: 'entertainment', label:'ENTRETENIMIENTO'},
        {value: 'health', label:'SALUD'},
        {value: 'science', label:'CIENCIA'},
        {value: 'sports', label:'DEPORTES'},
        {value: 'tecnology', label:'TECNOLOGIA'},
    ]

    // utilizar custom hook useSelect
    const [categoria, SelectNoticias] = useSelect('general', OPCIONES);

    // Submit al form,pasar categoria a app.js
    const buscarNoticias = e => {
        e.preventDefault();
        guardarCategoria(categoria);
    }

    return ( 
        <div className={`${styles.buscador} row`}>
            <div className="col s12 m8 offset-m2">
                <form
                    onSubmit={buscarNoticias}
                >
                    <h2 className={styles.heading}>Encuentra Noticias por Categoría</h2>
                    
                    <SelectNoticias></SelectNoticias>

                    <div className="input-field col s12">
                        <input 
                            type="submit" 
                            className={`${styles.btn_block} btn-large amber darken-2`}
                            value="BUSCAR"
                        />
                    </div>
                </form>
            </div>
        </div>
     );
}

Formulario.propTypes = {
    guardarCategoria: PropTypes.func.isRequired
}
 
export default Formulario
