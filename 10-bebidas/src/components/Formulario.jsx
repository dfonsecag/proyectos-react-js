import React, {useContext, useState} from "react";
import { CategoriasContext } from "../context/CategoriasContext";
import { RecetasContext } from "../context/RecetasContext";

const Formulario = () => {

    //state que almacena lo que el usuario selecciono
    const [busqueda, guardarBusqueda] = useState({
        nombre:'',
        categoria:''
    })
    //Context llama para cargar el select
    const {categorias} = useContext(CategoriasContext);
    
    // Context llama para busqueda de recetas
    const {buscarRecetas, guardarConsultar} = useContext(RecetasContext);

    //funcion para leer los datos formulario
    const obtenetDatosReceta = e => {
        guardarBusqueda({
            ...busqueda,
            [e.target.name] : e.target.value
        })
    }

  return (
    <form 
    className="col-12"
    onSubmit={e =>{
        e.preventDefault();
        buscarRecetas(busqueda)
        guardarConsultar(true)
    }}
    >
      <fieldset className="text-center">
        <legend>Buscar bebidas por categoria o ingrediente</legend>
      </fieldset>

      <div className="row mt-4">
        <div className="col-md-4">
          <input
            type="text"
            name="nombre"
            className="form-control"
            placeholder="Buscar por ingrediente"
            onChange={obtenetDatosReceta}
          />
        </div>

        <div className="col-md-4">
          <select
           name="categoria" 
           className="form-control"
           onChange={obtenetDatosReceta}
           >
            <option value="">-- Selecciona Categoria</option>
            {
                categorias.map(categoria =>(
                    <option key={categoria.strCategory} value={categoria.strCategory}>{categoria.strCategory}</option>
                ))
            }
          </select>
        </div>

        <div className="col-md-4">
          <input
            type="submit"
            className="btn btn-block btn-primary "
            value="Buscar Bebidas"
          />
        </div>

      </div>
    </form>
  );
};

export default Formulario;
