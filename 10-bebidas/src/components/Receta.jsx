import React, { useContext, useState } from "react";

import Modal from "@material-ui/core/Modal";

import { makeStyles } from "@material-ui/core/styles";

import { ModalContext } from "../context/ModalContext";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 450,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const Receta = ({ receta }) => {
  // Configuracion del Modal de materiaUI
  const [modalStyle] = useState(getModalStyle);
  // state si el modal esta abierto o cerrado
  const [open, setOpen] = useState(false);

  const classes = useStyles();

  const hadleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  // Extraer los valores del context
  const { inforeceta, guardarIdReceta, guardarInfoReceta} = useContext(ModalContext);

  // Muestra y formatea los ingredientes y cantidades
  const mostrarIngredientes = inforeceta => {
      let ingredientes = [];
      for (let i = 0; i < 16; i++) {
          if(inforeceta[`strIngredient${i}`]){
              ingredientes.push(
                  <li>{ inforeceta[`strIngredient${i}`] } { inforeceta[`strMeasure${i}`] }</li>
              )
          }    
      }
      return ingredientes;
  }

  return (
    <div className="col-md-4 mb-3 ">
      <div className="card">
        <h2 className="card-header">{receta.strDrink}</h2>

        <img
          className="card-img-top"
          src={receta.strDrinkThumb}
          alt={`Imagen de ${receta.strDrink}`}
        />

        <div className="card-body">
          <button
            type="button"
            className="btn btn-block btn-primary"
            onClick={() => {
              guardarIdReceta(receta.idDrink);
              hadleOpen();
            }}
          >
            Ver Receta
          </button>

          <Modal
            open={open}
            onClose={() => {
              guardarIdReceta(null);
              guardarInfoReceta({})
              handleClose();
            }}
          >
            <div style={modalStyle} className={classes.paper}>

              <h2>{inforeceta.strDrink}</h2>

              <h3 className="mt-4">Intrucciones</h3>

              <p>{inforeceta.strIntructions}</p>

              <img className="img-fluid my-4" src={inforeceta.strDrinkThumb} alt="" />

              <h3>Ingredientes y cantidades</h3>
              <ul>
                  {
                      mostrarIngredientes(inforeceta)
                  }
              </ul>
            </div>
          </Modal>
        </div>
      </div>
    </div>
  );
};

export default Receta;
