import React, { createContext, useState, useEffect } from "react";
import axios from "axios";

// Crear el context
export const CategoriasContext = createContext();

// Provides es donde se encuentran las funciones y state

const CategoriasProvider = (props) => {
  //creamos el state de context
  const [categorias, guardarCategorias] = useState([]);

  //hacemos el llamado a la api para traer categorias de bebidas
  useEffect(() => {
    const obtenerCategorias = async () => {
      const url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list";
      const categorias = await axios.get(url);
      guardarCategorias(categorias.data.drinks);
    };
    obtenerCategorias();
  }, []);

  return (
    <CategoriasContext.Provider 
    value={{
        categorias
    }}
    >
      {props.children}
    </CategoriasContext.Provider>
  );
};

export default CategoriasProvider;
