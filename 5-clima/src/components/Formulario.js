import React, { useState } from "react";
import PropTypes from 'prop-types'


import Error from "./Error";

const Formulario = ({busqueda, guardarBusqueda, guardarConsultar}) => {
 
  //state de error
  const [error, guardarError] = useState(false);

  const { ciudad, pais } = busqueda;

  // Funcion que coloca los elementes en el state

  const handleChange = (e) => {
    //actualizar el state
    guardarBusqueda({
      ...busqueda,
      [e.target.name]: e.target.value,
    });
  };

  // Cuando el usuario da submit al formulario

  const handleSubmit = (e) => {
    e.preventDefault();
    //validar
    if (ciudad.trim() === "" || pais.trim() === "") {
      guardarError(true);
      return;
    }

    guardarError(false);

    //cambiar state true para conultar a la api
    guardarConsultar(true);
    //pasar al componente principal
  };

  return (
    <form onSubmit={handleSubmit}>
        {
            error ? <Error mensaje='Ambos campos son requeridos'/> : null
        }
     
      <div className="input-field col s12">
        <input
          type="text"
          name="ciudad"
          id="ciudad"
          value={ciudad}
          onChange={handleChange}
        />
        <label htmlFor="ciudad">Ciudad: </label>
      </div>

      <div className="input-field col s12">
        <select name="pais" id="pais" value="pais" onChange={handleChange}>
          <option value="">-- Seleccione un pais --</option>
          <option value="US">Estados Unidos</option>
          <option value="MX">México</option>
          <option value="AR">Argentina</option>
          <option value="CO">Colombia</option>
          <option value="CR">Costa Rica</option>
          <option value="ES">España</option>
          <option value="PE">Perú</option>
        </select>
        <label htmlFor="pais">Pais: </label>
      </div>

      <div className="input-field col s12">
          <input type="submit" 
          className="waves-effect wave-light btn-large btn-block yellow accent-4" 
          value="Buscar Clima"/>
      </div>
    </form>
  );
};

Formulario.propTypes = {
  busqueda: PropTypes.object.isRequired, 
  guardarBusqueda: PropTypes.func.isRequired, 
  guardarConsultar: PropTypes.func.isRequired  
}

export default Formulario;
