import React, { Fragment, useState} from "react";
import PropTypes from 'prop-types'
import { v4 as uuidv4 } from 'uuid';

const Formulario = ({crearCita}) => {

    //Crear state de Citas
    const [cita, actualizarCita]= useState({
        mascota: '',
        propietario: '',
        fecha: '',
        hora: '',
        sintomas: ''

    })

    //Funcion qie se ejecuta cada vez que el usuario escribe en algun input

    const actualizarState = (e) => {
        actualizarCita({
            ...cita,
            //toma la objeto propiedad y le asigna el valor
            [e.target.name] : e.target.value
        })
    }

    const [error, actualizarError] = useState(false)

    // Extraer los valores
    const {mascota, propietario, fecha, hora, sintomas } = cita;

    //Cuando el usuario va enviar el formulario
    const submitCita = (e) => {
        e.preventDefault();
        // Validar
        if(mascota.trim() === '' || propietario.trim() === '' || fecha.trim() === '' || hora.trim() === '' || sintomas.trim() === '' ){
            actualizarError(true);
            return;
        }
        //eliminar el mensaje error si paso condicion
        actualizarError(false)

        //asignar Id
        cita.id = uuidv4()

        //Crear la cita
        crearCita(cita)

        //Reiniciar el form
        actualizarCita({
              mascota: '',
        propietario: '',
        fecha: '',
        hora: '',
        sintomas: ''
        })

    }


  return (
    <Fragment>
      <h2>Desde el formulario</h2>
      {
          error ?  <p className="alerta-error">Todos los campos son obligatorios</p> : null
      }
      <form
        onSubmit={submitCita}
      >
        <label>Nombre Mascota</label>
        <input
          type="text"
          name="mascota"
          className="u-full-width"
          placeholder="Nombre Mascota"
          onChange={actualizarState}
          value={mascota}
        />
        <label>Nombre Dueno</label>
        <input
          type="text"
          name="propietario"
          className="u-full-width"
          placeholder="Nombre Dueno de la mascota"
          onChange={actualizarState}
          value={propietario}
        />
        <label>Fecha</label>
        <input type="date" name="fecha" className="u-full-width" onChange={actualizarState} value={fecha}/>       
        <label>Hora</label>
        <input type="time" name="hora" className="u-full-width" onChange={actualizarState} value={hora}/>
        <label>Sintomas</label>
        <textarea className="u-full-width" name="sintomas" onChange={actualizarState} value={sintomas}></textarea>

        <button type="submit" className="u-full-width button-primary" >Agregar Cita</button>
      </form>
    </Fragment>
  );
};

Formulario.propTypes = {
    crearCita: PropTypes.func.isRequired
}

export default Formulario;
