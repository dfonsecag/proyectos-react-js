import React, { useState , useEffect} from "react";

import Pregunta from "./components/Pregunta";
import Formulario from "./components/Formulario";
import Listado from "./components/Listado";
import ControlPresupuesto from "./components/ControlPresupuesto";

function App() {
  //Definir presupuesto
  const [presupuesto, guardarPresupuesto] = useState(0);

  //guardar Restante dinero disponible
  const [restante, guardarRestante] = useState(0);

  //Definir presupuesto y luego no volver a mostrar
  const [mostrarpregunta, actualizarPregunta] = useState(true);

  //State para guardar los gastos del Formulario
  const [gastos, guardarGastos] = useState([]);
  // Guardar Gasto
  const [gasto, guardarGasto] = useState({});
  //El estado de crear gasto para que se ejecute useEfect
  const [creargasto, guardarCrearGasto] = useState(false);

  // Use Effect que se actualice el restante
  useEffect(() => {
   if(creargasto){
     //Agregar el nuevo presupuesto
    guardarGastos([
      ...gastos, gasto
    ]);
    //resta del presupuesto actual
    const presupuestoRestante = restante - gasto.cantidad;
    guardarRestante(presupuestoRestante);
    //resetear a false
    guardarCrearGasto(false);
   }
  },[gasto]);
  
  

  return (
    <div className="container">
      <header>
        <h1>Gasto Semanal</h1>
        <div className="contenido-principal contenido">
         {/* Valida que despues de dar el presupuesto no se muestre pregunta */}
          {
            mostrarpregunta ?
              (<Pregunta
                guardarPresupuesto={guardarPresupuesto}
                guardarRestante={guardarRestante}
                actualizarPregunta={actualizarPregunta}
              />)
              : (
                <div className="row">
                  <div className="one-half column">
                    <Formulario
                      guardarGasto={guardarGasto}
                      guardarCrearGasto={guardarCrearGasto}
                    />
                  </div>
                  <div className="one-half column">
                    <Listado
                      gastos={gastos}
                    />

                    <ControlPresupuesto
                      presupuesto={presupuesto}
                      restante={restante}
                    />
                      
                  </div>
                </div>
              )}
        </div>
      </header>
    </div>
  );
}

export default App;
