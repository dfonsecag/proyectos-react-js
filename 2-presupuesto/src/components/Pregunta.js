import React, { Fragment, useState} from "react";

import Error from "./Error";

const Pregunta = ({guardarPresupuesto, guardarRestante, actualizarPregunta}) => {

    //Definir el state
    const [cantidad, guardarCantidad] = useState(0)
    //Definir state error
    const [error, guardarError] = useState(false);
    //funcion que lee el presupuesto
    const definirPresupuesto = (e) => {
        //console.log(parseInt(e.target.value, 10))
       guardarCantidad(parseInt(e.target.value, 10))
    }
    //Submit para el presupuesto
    const agregarPresupuesto = e => {
        e.preventDefault();
        //validar
        if(cantidad < 1 || isNaN(cantidad)){
            guardarError(true)
            return;
        }

        //si pasa la validacion  
        guardarError(false);

        //cargar datos state presupuesto y restante de props
        guardarPresupuesto(cantidad);
        guardarRestante(cantidad);
        //pasar false pregunta presupuesto
        actualizarPregunta(false)
    }

  return (
    <Fragment>
      <h2> Coloca tu presupuesto</h2>
      {
          error ? <Error msj="El presupuesto es Incorrecto"/> : null
      }
      <form
        onSubmit={agregarPresupuesto}
      >
        <input
          type="number"
          className="u-full-width"
          placeholder="Coloca su presupuesto"
          onChange={definirPresupuesto}
        />

        <input
          type="submit"
          className="button-primary u-full-width"
          value="Definir Presupuesto"
        />
      </form>
    </Fragment>
  );
};

export default Pregunta;
