import React, { Fragment, useState, useEffect} from "react";
import styled from '@emotion/styled';

const Label = styled.label`
    font-family: 'Bebas Neue', cursive;
    color: #FFF;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 2.4rem;
    margin-top: 2rem;
    display: block;
`;

const Select = styled.select`
    width: 100%;
    display: block;
    padding: 1rem;
    --webkit-appearance: none;
    border-radius: 10px;
    border: none;
    font-size: 1.2rem;
`;

const useCriptomonedas = (label,stateInicial, opciones) => {
  // state de nuestro custom hook
  const [state, actualizarState] = useState(stateInicial);

  const SelectCripto = () => (
    <Fragment>
      <Label>{label}</Label>
      <Select onChange={ e => actualizarState(e.target.value)} value={state} name="" id="">
        <option value="">-- Seleccione  --</option>
        {
            opciones.map(opcion => {
                const {Id, Name, FullName} = opcion.CoinInfo;
               return( <option key={Id} value={Name}>{FullName}</option>)
            })
        }
      </Select>
    </Fragment>
  );

  //retornar state, interfaz y funcion que modifica el state

  return [state, SelectCripto, actualizarState];
};

export default useCriptomonedas;
