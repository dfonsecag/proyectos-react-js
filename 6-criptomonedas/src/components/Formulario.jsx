import React, {useEffect, useState} from 'react'
import styled from '@emotion/styled';
import axios from 'axios';

import useMoneda from '../hooks/useMoneda';
import useCriptomonedas from '../hooks/useCriptomoneda';
import Error from './Error';

const Boton = styled.input`
    margin-top: 20px;
    font-weight: bold;
    font-size: 20px;
    padding: 10px;
    background-color: #66a2fe;
    border: none;
    width: 100%;
    border-radius: 10px;
    color: #FFF;
    transition: background-color .3s eases;

    &hover{
        background-color: #326AC0;
        cursor: pointer;
    }
`;



const Formulario = ({guardarMoneda, guardarCriptomoneda}) => {

    // State del listado de Criptomonedas de la Api
    const [listacripto, guardarCriptomonedasApi] = useState([]);
    // State para manejar el error del Formulario
    const [error, guardarError] = useState(false);

    const MONEDAS = [
        {codigo: 'USD', nombre:'Dolar de Estado Unidos'},
        {codigo: 'MXN', nombre:'Peso Mexicano'},
        {codigo: 'EUR', nombre:'Euro'},
        {codigo: 'GDP', nombre:'Libra Esterlina'},
        {codigo: 'CRC', nombre:'Colon de Costa Rica'}
    ]

    // Utilizar mi propio Hook Usemoneda
    const [moneda, SelectMonedas] = useMoneda('Elige tu moneda', '', MONEDAS); 

    // Utilizar mi propio Hook criptomoneda
    const [criptomoneda, SelectCripto] = useCriptomonedas('Elige tu criptomoneda', '', listacripto); 

    //Ejecutar llamado a la API
    useEffect(() => {
        const consultarApi = async()=>{
            const url = `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD`;

            const resultado = await axios.get(url);

            guardarCriptomonedasApi(resultado.data.Data);
        }

        consultarApi();

    }, [])

    //Cuando el usuario hace submit al formulario
    const cotizarMoneda = e => {
        e.preventDefault();
        //validar si ambos campos estan llenos
        if(moneda ==='' || criptomoneda===''){
            guardarError(true);
            return;
        }
        
        //pasar los datos al componete principal
        guardarError(false);

        guardarCriptomoneda(criptomoneda);

        guardarMoneda(moneda);
        
    }

    return ( 
        <form 
            onSubmit={cotizarMoneda}
        >
            {error ? <Error mensaje='Todos los campos son obligatorios'/> : null}            
            <SelectMonedas />

            <SelectCripto />

            <Boton
                type="submit"
                value="Calcular"
            />
        </form>
     );
}
 
export default Formulario;