const mongoose = require('mongoose');
require('dotenv').config({path: 'variables.env'});

//Funcion para conectar la base de datos
const conectaDB = async () => {
    try {

        await mongoose.connect(process.env.DB_MONGO,{            
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        });

        console.log('base de datos online');
        
    } catch (error) {
        console.log(error);
        process.exit(1); // detener la app
    }
}

module.exports = conectaDB;

