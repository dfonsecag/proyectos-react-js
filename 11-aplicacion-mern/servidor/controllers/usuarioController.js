const { request, response } = require("express");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { validationResult } = require("express-validator");
const Usuario = require("../models/Usuario");

exports.crearUsuario = async (req, res = response) => {
  //revisamos si hay errores digitados por el usuario
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }
  // extraer email y password
  const { email, password } = req.body;

  try {
    // Revisar que el usuario registrado sea unico
    let usuario = await Usuario.findOne({ email });

    if (usuario) {
      return res.status(400).json({ msg: "El usuario ya existe" });
    }

    //guardar el nuevo usuario
    usuario = new Usuario(req.body);
    // encriptar el password
    const salt = await bcryptjs.genSalt(10);
    usuario.password = await bcryptjs.hash(password, salt);
    //guardar usuario
    await usuario.save();

    // Crear y firmar el JWT
    const payload = {
        usuario:{
            id:usuario.id
        }
    };
    // firmar el jwt
    jwt.sign(
      payload,
      process.env.SECRETA,
      {
        expiresIn: 36000,
      },
      (error, token) => {
        if (error) throw error;
        res.status(200).json({ token: token });
      }
    );
  } 
  catch (error) {
    console.log(error);
    res.status(400).send("Error al guardar usuario");
  }
};
