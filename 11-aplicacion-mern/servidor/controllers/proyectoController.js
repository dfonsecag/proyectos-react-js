const { response } = require("express");
const { validationResult } = require("express-validator");

const Proyecto = require("../models/Proyecto");
const Tarea = require("../models/Tarea");

// crear una nueva tarea
exports.crearProyecto = async (req, res = response) => {
  //revisamos si hay errores digitados por el usuario
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }
  try {
    //Creamos el proyecto
    const proyecto = new Proyecto(req.body);
    // Antes de guardar el creador via JWT
    proyecto.creador = req.usuario.id;
    // Guardamos el proyecto
    proyecto.save();
    res.status(200).json(proyecto);
  } catch (error) {
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};

// Obtiene todos los proyectos del usuario actual
exports.obtenerProyectos = async (req, res = response) => {
  try {
    const proyectos = await Proyecto.find({ creador: req.usuario.id }).sort({
      creado: -1,
    });
    res.json({ proyectos });
  } catch (x) {
    console.log(x);
    res.status(500).send("Hubo un error");
  }
};

// Actualizar un proyecto
exports.actualizarProyecto = async (req, res = response) => {
  //revisamos si hay errores digitados por el usuario
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }
  // extraer la informacion del proyecto
  const { nombre } = req.body;
  const nuevoProyecto = {};
  if (nombre) {
    nuevoProyecto.nombre = nombre;
  }

  try {
    // revisar el id
    let proyecto = await Proyecto.findById(req.params.id);
    
    //verificar el creador del proyecto
    if (proyecto.creador.toString() !== req.usuario.id) {
      return res.status(401).json({ msg: "No autorizado" });
    }

    // Actualizar
    proyecto = await Proyecto.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: nuevoProyecto },
      { new: true }
    );

    res.json({ proyecto });
  } catch (error) {
    console.log(error);
    res.status(500).send("error en el servidor");
  }
};

// Eliminar un proyecto por su id
exports.eliminarProyecto = async (req, res = response) => {
  //revisamos si hay errores digitados por el usuario
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }
  // extraer la informacion del proyecto
  const { nombre } = req.body;
  const nuevoProyecto = {};
  if (nombre) {
    nuevoProyecto.nombre = nombre;
  }

  try {
    // revisar el id
    let proyecto = await Proyecto.findById(req.params.id);
    
    //verificar el creador del proyecto
    if (proyecto.creador.toString() !== req.usuario.id) {
      return res.status(401).json({ msg: "No autorizado" });
    }

    // Eliminar
    proyecto = await Proyecto.findOneAndRemove({_id: req.params.id});
    res.json({ msg:'Proyecto Eliminado' });

  } catch (error) {
    console.log(error);
    res.status(500).send("error en el servidor");
  }
};
