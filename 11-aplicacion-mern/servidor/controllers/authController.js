const { response } = require("express");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { validationResult } = require("express-validator");
const Usuario = require("../models/Usuario");

exports.autenticarUsuario = async (req, res = response) => {
  //revisamos si hay errores digitados por el usuario
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }
  // extraer email y password
  const { email, password } = req.body;

  try {
    //Revisar que sea un usuario registrado
    let usuario = await Usuario.findOne({ email });
    if (!usuario) {
      return res.status(400).json({ msg: "El usuario no existe" });
    }
    // Revisar su password
    const passCorrecto = await bcryptjs.compare(password, usuario.password);
    if (!passCorrecto) {
      return res.status(400).json({ msg: "Password Incorrecto" });
    }
    // Si todo es correcto
    // Crear y firmar el JWT
    const payload = {
      usuario: {
        id: usuario.id,
      },
    };
    // firmar el jwt
    jwt.sign(
      payload,
      process.env.SECRETA,
      {
        expiresIn: 36000,
      },
      (error, token) => {
        if (error) throw error;
        res.status(200).json({ token: token });
      }
    );
  } catch (error) {
    console.log(error);
  }
};

// Obtiene que usuario esta autenticado
exports.usuarioAutenticado = async (req, res=response) => {
  try {
    const usuario = await Usuario.findById(req.usuario.id).select('-password');
    res.json({usuario})
    
  } catch (error) {
    console.log(error),
    res.status(500).json({msg: 'Hubo un error'})
  }
}
