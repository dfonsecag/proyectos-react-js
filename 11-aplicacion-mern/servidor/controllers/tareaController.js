const { response } = require("express");
const { validationResult } = require("express-validator");

const Proyecto = require("../models/Proyecto");
const Tarea = require("../models/Tarea");

// crea una nueva tarea
exports.crearTarea = async (req, res = response) => {
  //revisamos si hay errores digitados por el usuario
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }
  // extraer si el Proyecto existe
  const { proyecto } = req.body;

  try {
    const existeProyecto = await Proyecto.findById(proyecto);
    if (!existeProyecto) {
      return res.status(404).json({ msg: "Proyecto no encontrado" });
    }
    // revisar si el proyecto actual pertenece al usuario autenticado
    if (existeProyecto.creador.toString() !== req.usuario.id) {
      return res.status(401).json({ msg: "No autorizado" });
    }
    //Creamos la tarea
    const tarea = new Tarea(req.body);
    // Guardamos el proyecto
    await tarea.save();
    res.status(200).json(tarea);
  } catch (error) {
    console.log(error);
    res.status(500).send("Hubo un error");
  }
};

//Obtiene las tareas por proyecto
exports.obtenerTareas = async (req, res = response) => {
  try {
    // extraer si el Proyecto existe
    const { proyecto } = req.query;
    const existeProyecto = await Proyecto.findById(proyecto);
    if (!existeProyecto) {
      return res.status(404).json({ msg: "Proyecto no encontrado" });
    }
    // revisar si el proyecto actual pertenece al usuario autenticado
    if (existeProyecto.creador.toString() !== req.usuario.id) {
      return res.status(401).json({ msg: "No autorizado" });
    }
    // Obtener las tareas por proyecto
    const tareas = await Tarea.find({ proyecto });
    res.json({ tareas });
  } catch (error) {
    console.log(error);
    res.status(500).send("Hubo un error");
  }
};

// Actualizar una tarea
exports.actualizarTarea = async (req, res = response) => {
  try {
    
    // extraer datos del body
    const { proyecto, nombre, estado } = req.body;
    //Verificamos si la tarea existe
    let tarea = await Tarea.findById(req.params.id);
    if (!tarea) {
      return res.status(404).json({ msg: "No existe esa tarea" });
    }
    //extraer proyecto
    const existeProyecto = await Proyecto.findById(proyecto);
    // revisar si el proyecto actual pertenece al usuario autenticado
    if (existeProyecto.creador.toString() !== req.usuario.id) {
      return res.status(401).json({ msg: "No autorizado" });
    }

    

    // Actualizamos la tarea
    tarea = await Tarea.findOneAndUpdate({_id:req.params.id}, req.body, {new:true});

    res.json({tarea})

  } catch (error) {
    console.log(error);
    res.status(500).send("Hubo un error");
  }
};

// Eliminando una tarea
exports.eliminarTarea = async (req, res = response) => {
    try {
      // extraer datos del body
      const { proyecto } = req.query;
      //Verificamos si la tarea existe
      let tarea = await Tarea.findById(req.params.id);
      if (!tarea) {
        return res.status(404).json({ msg: "No existe esa tarea" });
      }
      //extraer proyecto
      const existeProyecto = await Proyecto.findById(proyecto);

      // revisar si el proyecto actual pertenece al usuario autenticado
      if (existeProyecto.creador.toString() !== req.usuario.id) {
        return res.status(401).json({ msg: "No autorizado" });
      }
  
      // Eliminando la tarea
      await Tarea.findOneAndRemove({_id : req.params.id});

      res.json({msg: "Tarea eliminada"})
  
    } catch (error) {
      console.log(error);
      res.status(500).send("Hubo un error");
    }
  };
  