const express = require('express');
require('dotenv').config({path: 'variables.env'});
const conectaDB = require('../servidor/config/db');

//importar cors para no haber problemas de dominio al ser localhost
const cors = require('cors');

// creamos el servidor
const app = express();

//conectar a la base de datos
conectaDB();

//habilitar Cors
app.use(cors())

// habilitar express.json
app.use(express.json({ extended: true }));

//definimos el puerto
const PORT = process.env.PORT || 4000;
 
// Importamos las rutas de usuarios
app.use('/api/usuarios', require('./routes/usuarios'))
// Importamos las rutas de autenticacion
app.use('/api/auth', require('./routes/auth'))
// Importamos las rutas de proyectos
app.use('/api/proyectos', require('./routes/proyectos'))
// Importamos las rutas de tareas
app.use('/api/tareas', require('./routes/tareas'))


//arrancar la app 
app.listen(PORT, () => {
    console.log(`El servidor esta corriendo en puerto: ${PORT}`)
})