import React, { useContext, useEffect } from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";

import Proyecto from "./Proyecto";
import proyectoContext from "../../context/proyectos/proyectoContext";
import AlertaContext from "../../context/alertas/alertaContext";

const ListadoProyectos = () => {
  // hago el llamado a proyecto context
  const proyectosContext = useContext(proyectoContext);
  // extraer los datos de proyecto state los proyectos
  const { mensaje,proyectos, obtenerProyectos } = proyectosContext;

  // hago el llamado a alerta context
  const alertaContext = useContext(AlertaContext);
  const { alerta, mostrarAlerta } = alertaContext;

  // Llamar funcion para obtener los proyectos
  useEffect(() => {

    // Si hubo un error
    if(mensaje){
      mostrarAlerta(mensaje.msg, mensaje.categoria);

    }

    obtenerProyectos();
    // eslint-disable-next-line
  }, [mensaje]);

  //revisar si proyectos tiene contenido
  if (proyectos.length === 0) {
    return <p>No hay proyectos, comience creando uno</p>;
  }

  return (
    <ul className="listado-proyectos">
      {alerta ? <p className={`${alerta.categoria}`}>{alerta.msg}</p> : null}
      <TransitionGroup>
        {proyectos.map((proyecto) => (
          <CSSTransition key={proyecto._id} timeout={200} classNames="proyecto">
            <Proyecto proyecto={proyecto} />
          </CSSTransition>
        ))}
      </TransitionGroup>
    </ul>
  );
};

export default ListadoProyectos;
