import React, { Fragment, useContext, useState } from "react";

import proyectoContext from "../../context/proyectos/proyectoContext";

const NuevoProyecto = () => {

  //obtener el state del formulario
  const proyectosContext = useContext(proyectoContext);

  const {formulario, errorformulario, mostrarFormulario, agregarProyecto, mostrarError} = proyectosContext;

  // state para nuevo proyecto guardar datos del formulario
  const [proyecto, guardarProyecto] = useState({
    nombre: "",
  });

  // Extraer datos
  const {nombre} = proyecto;

  // funcion onchange para ir almacenando los datos formulario
  const onChangeProyecto = (e) => {
    guardarProyecto({
      ...proyecto,
      [e.target.name]: e.target.value,
    });
  };

  // Funcion onSubmit Proyecto
  const onSubmitProyecto = e => {
      e.preventDefault();

      //validar
      if(nombre === ''){
        mostrarError();
        return;
      }


      //agregar el state
      agregarProyecto(proyecto);

      //reiniciar el formulario
      guardarProyecto({
        nombre:''
      })
  }

  return (
    <Fragment>
      <button 
      type="button" 
      className="btn btn-block btn-primario"
      onClick={() => mostrarFormulario()}
      >
        Nuevo Proyecto
      </button>

    {
      formulario ?
      (
        <form 
        onSubmit={onSubmitProyecto}
        className="formulario-nuevo-proyecto"
        >
          <input
            type="text"
            className="input-text"
            placeholder="Nombre proyecto"
            name="nombre"
            value={nombre}
            onChange={onChangeProyecto}
          />
  
          <input
            type="submit"
            className="btn btn-primario btn-block"
            value="Agregar Proyecto"
          />
        </form>
      )
      :null
    }

    {
      errorformulario ? <p className="mensaje error">El nombre del proyecto es obligatorio</p>  :null
    }
    </Fragment>
  );
};

export default NuevoProyecto;
