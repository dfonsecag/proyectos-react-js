import React, {useState} from 'react'

import Error from './Error';

const Formulario = ({guardarBusqueda}) => {

    // estado del input de formulario
    const [termino, guardarTermino] = useState('');
    //guardar error en caso del input de busqueda este vacio
    const [error, guardarError] = useState(false)

    //Funcion al ejecutar el onsubmit
    const buscarImagenes = e => {
        e.preventDefault();
        //validar
        if(termino.trim() ===  ''){
            guardarError(true)
            return ;
        }
        guardarError(false);
        // enviar el termino de busqueda hacia el app.js
        guardarBusqueda(termino);
    }


    return ( 
        <form 
            onSubmit={buscarImagenes}
        >
            <div className="row">
                <div className="form-group col-md-8">
                    <input 
                    type="text" 
                    className="form-control form-control-lg"
                    placeholder="Busca una imagen, ejemplo futbol"
                    onChange={(e)=> guardarTermino(e.target.value)}
                    />
                </div>

                <div className="form-group col-md-4">
                    <input 
                    type="submit" 
                    className="btn btn-lg btn-danger btn-block"
                    value="Buscar"
                    
                    />
                </div>

            </div>

            {error ? <Error mensaje='Agrega un termino de busqueda'/> : null}

        </form>
     );
}
 
export default Formulario;